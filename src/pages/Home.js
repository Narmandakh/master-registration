import { connect } from 'react-redux'
import { useState } from 'react'
import { makeStyles } from '@material-ui/styles'
import {
    Box,
    Button,
    Grid,
    Container,
    Typography,
} from '@material-ui/core'
import { NavLink } from 'react-router-dom'

const useStyles = makeStyles({
    cover: {
        backgroundImage: 'url(/static/images/shutis-cover.jpg)',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeats',
        color: '#fff',
    },
})

const Home = props => {
    const [features] = useState([
        {
            name: 'Ipsum dolor sit',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id orci et lorem semper rhoncus sed a lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
            icon: 'clock.png'
        },
        {
            name: 'Ipsum dolor sit',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id orci et lorem semper rhoncus sed a lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
            icon: 'clock.png'
        },
        {
            name: 'Ipsum dolor sit',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id orci et lorem semper rhoncus sed a lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
            icon: 'clock.png'
        },
        {
            name: 'Ipsum dolor sit',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id orci et lorem semper rhoncus sed a lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
            icon: 'clock.png'
        }
    ])

    const {
        cover,
    } = useStyles()

    return (
        <>
            <Box py={12} className={cover}>
                <Container>
                    <Box mb={1} color="#ffffff80">
                        <Typography>МАГИСТР</Typography>
                    </Box>
                    <Typography variant="h3">Шинэ элсэгчийн бүртгэл.</Typography>
                    <Box
                        width="40%"
                        mt={5}
                        mb={6}
                    >
                        <Typography>МУБИС-ийн Шинэ элсэгч 2020 оны 08-р сарын 12-оос суралцах эрхийн бичиг, ЭЕШ-ын батламж, бүрэн дунд боловсролын гэрчилгээ, хувийн хэрэгтэй ирж бүртгүүлнэ үү.</Typography>
                    </Box>
                    {!props.student && (
                        <Box>
                            <Button
                                style={{
                                    backgroundColor: '#1b1d5a',
                                    boxShadow: '0 5px 15px #1b1d5a'
                                }}
                                variant="contained"
                                color="primary"
                                size="large"
                                to="/register"
                                component={NavLink}
                            >Бүртгүүлэх</Button>
                        </Box>
                    )}
                </Container>
            </Box>
            <Box py={12}>
                <Container>
                    <Grid
                        container
                        alignItems="center"
                    >
                        <Grid item xs={7}>
                            <img
                                width="600"
                                alt="computer"
                                src="/static/images/undraw_programmer_imem.png"
                            />
                        </Grid>
                        <Grid item xs>
                            <Box mb={1} color="#00000080">
                                <Typography>ҮЙЛЧИЛГЭЭ</Typography>
                            </Box>
                            <Typography variant="h5">ШУТИС-ийн нэгдсэн бүртгэл</Typography>
                            <Box mt={4}>
                                <Typography color="textSecondary">Бүртгэлийн системд регистрийн дугаар, цахим шуудан ашиглан хэрэглэгчийн нэг бүртгэл үүсгэх ба тухайн бүртгэлээ ашиглан ШУТИС-ийн магистрын элсэлтийн бүртгэлд хүсэлт гаргах боломжтой.</Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
            <Box py={12} bgcolor="#1b1d5a">
                <Container>
                    <Box
                        textAlign="center"
                        color="#fff"
                        mb={10}
                    >
                        <Box color="#ffffff80">
                            <Typography>МАНАЙ СУРГУУЛЬД</Typography>
                        </Box>
                        <Typography variant="h4">Элссэнээр та дараах <br />боломжууд олж авах боломжтой</Typography>
                    </Box>
                    <Grid
                        container
                        spacing={5}
                    >
                        {features.map(({ name, description, icon }, index) =>
                            <Grid item xs={6} key={index}>
                                <Box
                                    bgcolor="#ffffff0d"
                                    color="#fff"
                                    borderRadius={10}
                                    padding={4}
                                >
                                    <Grid
                                        container
                                        spacing={3}
                                    >
                                        <Grid item xs={2}>
                                            <img
                                                width="90%"
                                                alt="feature"
                                                src={`/static/images/${icon}`}
                                            />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant="h6">
                                                {name}
                                            </Typography>
                                            <Box mt={2} color="#ffffff80">
                                                <Typography>
                                                    {description}
                                                </Typography>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Grid>
                        )}
                    </Grid>
                </Container>
            </Box>
            <Box py={12}>
                <Container>
                    <Grid
                        container
                        alignItems="center"
                        justify="space-between"
                    >
                        <Grid item xs={5}>
                            <Box mb={1} color="#00000080">
                                <Typography>ДАВУУ ТАЛ</Typography>
                            </Box>
                            <Typography variant="h5">Ухаалаг төхөөрөмжийг ашиглах боломж</Typography>
                            <Box mt={4}>
                                <Typography color="textSecondary">Компьютер, таблет, гар утас гэх мэт ухаалаг төхөөрөмжүүдээс хандан бүртгэлийн нэгдсэн системийг бүрэн ашиглах боломжтой.</Typography>
                            </Box>
                        </Grid>
                        <Grid item>
                            <img
                                width="600"
                                alt="device"
                                src="/static/images/undraw_in_sync_xwsa.png"
                            />
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </>
    )
}

const mapStateToProps = state => ({
    student: state.student
})

export default connect(mapStateToProps)(Home)