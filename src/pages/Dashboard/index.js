import Admission from './Admission'
import Admissions from './Admissions'
import Home from './Home'
import Login from './Login'

export {
    Admission,
    Admissions,
    Home,
    Login,
}