import { useParams } from 'react-router-dom'
import {
    useState,
    useEffect
} from 'react'
import {
    Container,
    Box,
    Button,
    Grid,
    Typography,
    Divider
} from '@material-ui/core'
import firebase from './../../plugins/firebase'

const Admission = () => {
    const [user, setUser] = useState({})
    const [files, setFiles] = useState([])

    const { id } = useParams()

    const fetchUser = () => {
        try {
            firebase.database().ref(`students/${id}`)
                .on('value', (snapshot) => {
                    setUser(snapshot.val())
                })

        } catch (err) {
            // 
        }
    }

    const fetchFiles = () => {
        try {
            firebase.database().ref(`admissions/${id}`)
                .on('value', (snapshot) => {
                    setFiles(snapshot.val().files)
                })

        } catch (err) {
            // 
        }
    }

    const handleDownloadFile = async path => {
        const url = await firebase.storage().ref(path).getDownloadURL()
        window.open(url, '_blank')
    }

    useEffect(() => {
        fetchUser()
        fetchFiles()
    }, [])

    return (
        <Box mt={5}>
            <Container maxWidth="sm">
                <Box
                    borderRadius={10}
                    bgcolor="#fff"
                    padding={3}
                >
                    <Grid
                        container
                        spacing={3}
                    >
                        <Grid item xs={6}>
                            <Typography
                                color="textSecondary"
                                variant="subtitle2"
                            >Овог</Typography>
                            <Typography>
                                {user.surname}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography
                                color="textSecondary"
                                variant="subtitle2"
                            >Нэр</Typography>
                            <Typography>
                                {user.name}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography
                                color="textSecondary"
                                variant="subtitle2"
                            >Имэйл</Typography>
                            <Typography>
                                {user.email}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Box my={2}>
                        <Divider />
                    </Box>
                    <Box>
                        <Box>
                            <Typography
                                color="textSecondary"
                                variant="subtitle2"
                            >Элсэгчийн материалууд</Typography>
                        </Box>
                        {files.map(file =>
                            <Button
                                color="primary"
                                onClick={() => handleDownloadFile(file)}
                            >
                                {file}
                            </Button>
                        )}
                    </Box>
                    <Box my={2}>
                        <Divider />
                    </Box>
                    <Grid
                        container
                        spacing={2}
                        justify="flex-end"
                    >
                        <Grid item>
                            <Button
                                color="secondary"
                                variant="outlined"
                            >Цуцлах</Button>
                        </Grid>
                        <Grid item>
                            <Button
                                color="primary"
                                variant="outlined"
                            >Батлах</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
        </Box>
    )
}

export default Admission