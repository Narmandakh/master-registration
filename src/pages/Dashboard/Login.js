import { connect } from 'react-redux'
import { setAdmin } from './../../redux/auth/actions'
import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import {
    Box,
    Button,
    TextField,
    CircularProgress,
} from '@material-ui/core'
import Auth from './../../layouts/Auth'
import firebase from './../../plugins/firebase'

const Login = props => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [disabled, setDisabled] = useState(false)

    const history = useHistory()

    const handleLogin = async e => {
        e.preventDefault()

        setDisabled(true)

        try {
            const { user } = await firebase.auth().signInWithEmailAndPassword(
                email,
                password
            )

            props.setAdmin(user)

            history.push('/d')
        } catch (err) {
            console.log(err)
            setDisabled(false)
        }
    }

    return (
        <Auth>
            <form onSubmit={handleLogin}>
                <TextField
                    fullWidth
                    label="Имэйл"
                    variant="outlined"
                    margin="normal"
                    type="email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                <TextField
                    fullWidth
                    label="Нууц үг"
                    variant="outlined"
                    margin="normal"
                    type="password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
                <Box my={3}>
                    <Button
                        disabled={disabled}
                        fullWidth
                        type="submit"
                        variant="contained"
                        color="primary"
                        size="large"
                    >
                        {disabled ? <CircularProgress size={23} /> : 'Нэвтрэх'}
                    </Button>
                </Box>
            </form>
        </Auth>
    )
}

const mapDispatchToProps = {
    setAdmin
}

export default connect(null, mapDispatchToProps)(Login)