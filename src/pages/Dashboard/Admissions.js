import {
    useState,
    useEffect
} from 'react'
import {
    Box,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    IconButton
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import VisibilityIcon from '@material-ui/icons/Visibility'
import firebase from './../../plugins/firebase'

const Admissions = () => {
    const [students, setStudents] = useState([])

    const fetchData = async () => {
        try {
            await firebase.database().ref('admissions')
                .on('value', (snapshot) => {
                    const admissions = Object.keys(snapshot.val())
                    firebase.database().ref('students')
                        .on('value', (snapshot) => {
                            const data = Object.entries(snapshot.val())
                            setStudents(data.filter(val => admissions.includes(val[0])))
                        })
                })
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <Box>
            <Box mb={5}>
                <Typography variant="h4">Элсэгч</Typography>
            </Box>
            <Box
                bgcolor="#fff"
                borderRadius={10}
            >
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Овог</TableCell>
                                <TableCell>Нэр</TableCell>
                                <TableCell>Имэйл</TableCell>
                                <TableCell>Дэлгэрэнгүй</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {students.map((student, index) => (
                                <TableRow key={index}>
                                    <TableCell>
                                        {student[1].surname}
                                    </TableCell>
                                    <TableCell>
                                        {student[1].name}
                                    </TableCell>
                                    <TableCell>
                                        {student[1].email}
                                    </TableCell>
                                    <TableCell>
                                        <IconButton
                                            to={`/d/admissions/${student[0]}`}
                                            component={Link}
                                        >
                                            <VisibilityIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
        </Box>
    )
}

export default Admissions