import Admission from './Admission'
import ChangePassword from './ChangePassword'
import Home from './Home'
import Login from './Login'
import Register from './Register'

export {
    Admission,
    ChangePassword,
    Home,
    Login,
    Register,
}