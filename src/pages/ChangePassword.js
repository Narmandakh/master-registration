import firebase from './../plugins/firebase'
import { useState } from 'react'
import { NavLink } from 'react-router-dom'
import {
    Button,
    Box,
    TextField,
    Grid,
    CircularProgress,
    Snackbar,
    Container,
    Typography
} from '@material-ui/core'
import { Alert } from '@material-ui/lab'

const ChangePassword = props => {
    const [currentPassword, setCurrentPassword] = useState('')
    const [password, setPassword] = useState('')
    const [passwordConfirm, setPasswordConfirm] = useState('')
    const [disabled, setDisabled] = useState(false)
    const [changed, setChanged] = useState(false)

    const handleChangePassword = async e => {
        e.preventDefault()

        setDisabled(true)

        try {
            await firebase.auth().currentUser.updatePassword(password)

            setDisabled(false)
            setChanged(true)
        } catch (err) {
            setDisabled(false)
            console.log(err)
        }
    }

    return (
        <Box bgcolor="#f4f5f7" py={15}>
            <Container>
                <Grid
                    container
                    spacing={5}
                    justify="space-between"
                >
                    <Grid item xs={5}>
                        <Typography variant="h4">Нууц үг солих</Typography>
                        <Box mt={3}>
                            <Typography color="textSecondary">
                                {'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam risus non venenatis gravida. Nulla facilisi. Vivamus tincidunt, ligula ut pulvinar convallis, lectus tortor elementum ligula, a iaculis risus justo porta dui. '}
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={5}>
                        <Box
                            bgcolor="#fff"
                            padding={3}
                            borderRadius={10}
                            boxShadow="0 5px 10px #ddd"
                        >
                            <form onSubmit={handleChangePassword}>
                                <TextField
                                    fullWidth
                                    label="Одоогийн нууц үг"
                                    variant="outlined"
                                    margin="normal"
                                    type="password"
                                    value={currentPassword}
                                    onChange={e => setCurrentPassword(e.target.value)}
                                />
                                <TextField
                                    fullWidth
                                    label="Шинэ нууц үг"
                                    variant="outlined"
                                    margin="normal"
                                    type="password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                                <TextField
                                    fullWidth
                                    label="Шинэ нууц үг баталгаажуулах"
                                    variant="outlined"
                                    margin="normal"
                                    type="password"
                                    value={passwordConfirm}
                                    onChange={e => setPasswordConfirm(e.target.value)}
                                />
                                <Box my={3}>
                                    <Button
                                        disabled={disabled}
                                        fullWidth
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        size="large"
                                    >
                                        {disabled ? <CircularProgress size={23} /> : 'Нэвтрэх'}
                                    </Button>
                                </Box>
                                <Button
                                    color="primary"
                                    to="/register"
                                    component={NavLink}
                                    style={{ textTransform: 'none' }}
                                >
                                    {"Нэвтрэх хаяг үүсгэх?"}
                                </Button>
                            </form>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
            <Snackbar
                onClose={() => setChanged(false)}
                open={changed}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
            >
                <Alert
                    onClose={() => setChanged(false)}
                    variant="filled"
                    severity="info"
                >
                    Таны нууц үг амжилттай шинэчлэгдлээ
                </Alert>
            </Snackbar>
        </Box>
    )
}

export default ChangePassword