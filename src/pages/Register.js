import firebase from './../plugins/firebase'
import { useState } from 'react'
import { NavLink } from 'react-router-dom'
import {
    Button,
    Box,
    TextField,
    Grid,
    CircularProgress,
    Container,
    Typography,
    Snackbar,
} from '@material-ui/core'
import {
    Alert,
    AlertTitle
} from '@material-ui/lab'

const Register = props => {
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [disabled, setDisabled] = useState(false)
    const [snack, setSnack] = useState(false)

    const handleRegister = async e => {
        e.preventDefault()

        setDisabled(true)

        try {
            const res = await firebase.auth().createUserWithEmailAndPassword(
                email,
                password
            )

            await firebase.auth().currentUser.sendEmailVerification()

            await firebase.database().ref(`students/${res.user.uid}`).set({
                name,
                surname,
                email,
            })

            setDisabled(false)
            setSnack(true)
        } catch (err) {
            setDisabled(false)
            console.log(err)
        }
    }

    return (
        <Box bgcolor="#f4f5f7" py={15}>
            <Container>
                <Grid
                    container
                    spacing={5}
                    justify="space-between"
                >
                    <Grid item xs={5}>
                        <Typography variant="h4">Нэвтрэх хаяг үүсгэх</Typography>
                        <Box mt={3}>
                            <Typography color="textSecondary">
                                {'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam risus non venenatis gravida. Nulla facilisi. Vivamus tincidunt, ligula ut pulvinar convallis, lectus tortor elementum ligula, a iaculis risus justo porta dui. '}
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={5}>
                        <Box
                            bgcolor="#fff"
                            padding={3}
                            borderRadius={10}
                            boxShadow="0 5px 10px #ddd"
                        >
                            <form onSubmit={handleRegister}>
                                <TextField
                                    fullWidth
                                    label="Овог"
                                    variant="outlined"
                                    margin="normal"
                                    value={surname}
                                    onChange={e => setSurname(e.target.value)}
                                />
                                <TextField
                                    fullWidth
                                    label="Нэр"
                                    variant="outlined"
                                    margin="normal"
                                    value={name}
                                    onChange={e => setName(e.target.value)}
                                />
                                <TextField
                                    fullWidth
                                    label="Имэйл"
                                    variant="outlined"
                                    margin="normal"
                                    type="email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                />
                                <TextField
                                    fullWidth
                                    label="Нууц үг"
                                    variant="outlined"
                                    margin="normal"
                                    type="password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                                <Box my={3}>
                                    <Button
                                        disabled={disabled}
                                        fullWidth
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        size="large"
                                    >
                                        {disabled ? <CircularProgress size={23} /> : 'Бүртгүүлэх'}
                                    </Button>
                                </Box>
                                <Button
                                    color="primary"
                                    to="/login"
                                    component={NavLink}
                                    style={{ textTransform: 'none' }}
                                >
                                    {'Бүртгэлтэй хаягаар нэвтрэх?'}
                                </Button>
                            </form>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
            <Snackbar
                onClose={() => setSnack(false)}
                open={snack}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
            >
                <Alert
                    onClose={() => setSnack(false)}
                    variant="filled"
                    severity="success"
                >
                    <AlertTitle>Таны бүртгэл амжилттай хийгдлээ</AlertTitle>
                    Та цахим шуудангаа баталгаажуулан нэвтэрч орно уу.
                </Alert>
            </Snackbar>
        </Box>
    )
}

export default Register