import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import {
    useState,
    useEffect
} from 'react'
import {
    Box,
    Button,
    Container,
    Divider,
    Grid,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
} from '@material-ui/core'
import {
    DropZoneMultiple,
    Tabs,
} from './../components'
import { makeStyles } from '@material-ui/styles'
import firebase from './../plugins/firebase'

const useStyles = makeStyles({
    cover: {
        backgroundImage: 'url(https://images.unsplash.com/photo-1489486501123-5c1af10d0be6?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2850&q=80)',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        padding: 30,
        paddingTop: 70,
        color: '#fff'
    }
})

const Admission = props => {
    const [programs, setPrograms] = useState([])
    const [files, setFiles] = useState([])
    const [requestOpen, setRequestOpen] = useState(false)
    const [dialogDisabled, setDialogDisabled] = useState(false)

    const { cover } = useStyles()
    const history = useHistory()

    const fetchData = async () => {
        try {
            const uid = await firebase.auth().currentUser.uid

            firebase.database().ref(`admissions/${uid}`)
                .on('value', (snapshot) => {
                    const data = snapshot.val()

                    setFiles(data.files)
                })
        } catch (err) {
            console.log(err)
        }
    }

    const fetchPrograms = async () => {
        try {
            firebase.database().ref('programs')
                .on('value', (snapshot) => {
                    setPrograms(snapshot.val())
                })
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        fetchData()
        fetchPrograms()
    }, [])

    const handleSendRequest = async () => {
        setDialogDisabled(true)

        try {
            const uid = await firebase.auth().currentUser.uid

            await firebase.database().ref(`admissions/${uid}`).set({ files })

            setDialogDisabled(false)
            setRequestOpen(false)
        } catch (err) {
            console.log(err)
        }
    }

    const openRequestDialog = () => {
        if (!props.student) history.push('/login')

        setRequestOpen(true)
    }

    const summary = (
        <Box
            bgcolor="#fff"
            borderRadius={10}
            boxShadow="0 5px 10px #ddd"
            overflow="hidden"
        >
            <Box className={cover}>
                <Typography variant="h5">Магистрын элсэлт</Typography>
            </Box>
            <Box padding={3}>
                <Grid container justify="space-between">
                    <Grid item>
                        <Typography
                            variant="subtitle2"
                            color="textSecondary"
                        >Эхлэх огноо</Typography>
                        <Typography>2020/11/10 08:30</Typography>
                    </Grid>
                    <Grid item>
                        <Typography
                            variant="subtitle2"
                            color="textSecondary"
                        >Дуусах огноо</Typography>
                        <Typography>2021/01/10 17:30</Typography>
                    </Grid>
                </Grid>
                <Box my={3}>
                    <Divider />
                </Box>
                <Button
                    disabled={files.length}
                    onClick={openRequestDialog}
                    // style={{
                    //     backgroundColor: '#1b1d5a',
                    //     boxShadow: '0 2px 7px #1b1d5a'
                    // }}
                    variant="contained"
                    color="primary"
                    size="large"
                    fullWidth
                >Хүсэлт гаргах</Button>
            </Box>
        </Box>
    )

    const admissionAbout = (
        <>
            <Typography
                variant="h6"
                color="textSecondary"
            >Тавигдах шаардлага</Typography>
            <ul>
                <li>Бакалаврын дипломтой байх</li>
                <li>Салбартаа 2-оос дээш жил ажилласан байх</li>
            </ul>
            <Typography
                variant="h6"
                color="textSecondary"
            >Бүрдүүлэх материал</Typography>
            <ul>
                <li>Дипломын хуулбар</li>
                <li>Байгууллагын тодорхойлолт</li>
            </ul>
        </>
    )

    const admissionProgram = (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Сургууль</TableCell>
                        <TableCell>Хөтөлбөр</TableCell>
                        <TableCell align="center">Хяналтын тоо</TableCell>
                        <TableCell align="center">Бүртгэлийн хураамж</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {programs && programs.map(program =>
                        <TableRow>
                            <TableCell>
                                {program.school}
                            </TableCell>
                            <TableCell>
                                {program.name}
                            </TableCell>
                            <TableCell align="center">
                                {program.limit}
                            </TableCell>
                            <TableCell align="center">
                                {program.price}
                            </TableCell>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </TableContainer>
    )

    const admissionContact = (
        <>
            <Box my={2}>
                <Typography>8-р хороо, Бага тойруу, Сүхбаатар дүүрэг, Улаанбаатар, Монгол улс 14191</Typography>
            </Box>
            <Box my={2}>
                <Typography color="textSecondary">Утас</Typography>
                <Typography>(976)-11-324590</Typography>
            </Box>
            <Box my={2}>
                <Typography color="textSecondary">Е-Майл</Typography>
                <Typography>news@must.edu.mn</Typography>
            </Box>
        </>
    )

    const handleFileChange = (path, bool) => {
        let copyFiles = []
        if (bool === undefined) {
            setFiles(prevFiles => [...prevFiles, path])
        } else {
            copyFiles = files.filter(val => val !== path)
            setFiles(copyFiles)
        }
    }

    const requestForm = (
        <Dialog
            onClose={() => setRequestOpen(false)}
            open={requestOpen}
            maxWidth="sm"
            fullWidth
        >
            <DialogTitle>Хүсэлт гаргах</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {'Та материалуудаа доор хавсарган илгээнэ үү!'}
                </DialogContentText>
                <DropZoneMultiple
                    refPath="files"
                    files={files}
                    onChange={handleFileChange}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={() => setRequestOpen(false)}
                    color="primary"
                >Болих</Button>
                <Button
                    onClick={handleSendRequest}
                    color="primary"
                    disabled={dialogDisabled || files.length < 2}
                >Илгээх</Button>
            </DialogActions>
        </Dialog>
    )

    const details = (
        <Box
            bgcolor="#fff"
            borderRadius={10}
            boxShadow="0 5px 10px #ddd"
            overflow="hidden"
        >
            <Tabs
                items={[
                    'Тухай',
                    'Хөтөлбөр',
                    'Холбоо барих'
                ]}
                panels={[
                    admissionAbout,
                    admissionProgram,
                    admissionContact,
                ]}
            />
        </Box>
    )

    return (
        <Box bgcolor="#f4f5f7" py={5}>
            <Container>
                <Grid
                    container
                    spacing={5}
                >
                    <Grid item xs>
                        {details}
                    </Grid>
                    <Grid item xs={4}>
                        {summary}
                    </Grid>
                </Grid>
            </Container>
            {requestForm}
        </Box>
    )
}

const mapStateToProps = state => ({
    student: state.student
})

export default connect(mapStateToProps)(Admission)