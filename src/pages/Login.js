import { connect } from 'react-redux'
import { useState } from 'react'
import { setStudent } from './../redux/auth/actions'
import firebase from './../plugins/firebase'
import {
    NavLink,
    useHistory
} from 'react-router-dom'
import {
    Button,
    Box,
    TextField,
    Grid,
    CircularProgress,
    Snackbar,
    Container,
    Typography
} from '@material-ui/core'
import {
    Alert,
    AlertTitle
} from '@material-ui/lab'

const Login = props => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [disabled, setDisabled] = useState(false)
    const [verify, setVerify] = useState(false)

    const history = useHistory()

    const handleLogin = async e => {
        e.preventDefault()

        setDisabled(true)

        try {
            const { user } = await firebase.auth().signInWithEmailAndPassword(
                email,
                password
            )

            if (!user.emailVerified) {
                setVerify(true)
                setDisabled(false)
                return;
            }

            props.setStudent(user)

            history.push('/')
        } catch (err) {
            setDisabled(false)
            console.log(err)
        }
    }

    return (
        <Box bgcolor="#f4f5f7" py={15}>
            <Container>
                <Grid
                    container
                    spacing={5}
                    justify="space-between"
                >
                    <Grid item xs={5}>
                        <Typography variant="h4">Нэвтрэх</Typography>
                        <Box mt={3}>
                            <Typography color="textSecondary">
                                {'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam risus non venenatis gravida. Nulla facilisi. Vivamus tincidunt, ligula ut pulvinar convallis, lectus tortor elementum ligula, a iaculis risus justo porta dui. '}
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={5}>
                        <Box
                            bgcolor="#fff"
                            padding={3}
                            borderRadius={10}
                            boxShadow="0 5px 10px #ddd"
                        >
                            <form onSubmit={handleLogin}>
                                <TextField
                                    fullWidth
                                    label="Имэйл"
                                    variant="outlined"
                                    margin="normal"
                                    type="email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                />
                                <TextField
                                    fullWidth
                                    label="Нууц үг"
                                    variant="outlined"
                                    margin="normal"
                                    type="password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                                <Box my={3}>
                                    <Button
                                        disabled={disabled}
                                        fullWidth
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        size="large"
                                    >
                                        {disabled ? <CircularProgress size={23} /> : 'Нэвтрэх'}
                                    </Button>
                                </Box>
                                <Button
                                    color="primary"
                                    to="/register"
                                    component={NavLink}
                                    style={{ textTransform: 'none' }}
                                >
                                    {"Нэвтрэх хаяг үүсгэх?"}
                                </Button>
                            </form>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
            <Snackbar
                onClose={() => setVerify(false)}
                open={verify}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
            >
                <Alert
                    onClose={() => setVerify(false)}
                    variant="filled"
                    severity="info"
                >
                    <AlertTitle>Имэйл хаягаа баталгаажуулна уу</AlertTitle>
                    Та баталгаажуулалт хийсний дараа цааш үргэлжлүүлэх боломжтой болно.
                </Alert>
            </Snackbar>
        </Box>
    )
}

const mapDispatchToProps = {
    setStudent
}

export default connect(null, mapDispatchToProps)(Login)