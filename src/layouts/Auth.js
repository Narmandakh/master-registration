import {
    Avatar,
    Box,
    Container,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles({
    avatar: {
        width: 60,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: 0
    },
})

const Auth = ({ children }) => {
    const { avatar } = useStyles()

    return (
        <Container maxWidth="xs">
            <Box mt={10} mb={3}>
                <Avatar
                    alt="Avatar"
                    src="/static/images/logo.png"
                    className={avatar}
                />
            </Box>
            {children}
        </Container>
    )
}

export default Auth