import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { AppRoutes } from './../routes'
import { MainDrawer } from './../components'
import { Grid, Box } from '@material-ui/core'

const App = props => {
    if (!props.admin) {
        return <Redirect to="/d/login" />
    }

    return (
        <Grid container>
            <Grid item>
                <MainDrawer />
            </Grid>
            <Grid item xs>
                <Box
                    bgcolor="#f4f5f7"
                    padding={6}
                    height="calc(100vh - 96px)"
                >
                    <AppRoutes />
                </Box>
            </Grid>
        </Grid >
    )
}

const mapStateToProps = state => ({
    admin: state.admin
})

export default connect(mapStateToProps)(App)