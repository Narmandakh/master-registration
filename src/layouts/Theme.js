import { ThemeRoutes } from './../routes'
import {
    Navbar,
    Footer
} from './../components'

const Theme = props => {
    return (
        <>
            <Navbar />
            <ThemeRoutes />
            <Footer />
        </>
    )
}

export default Theme