import {
    Switch,
    Route,
} from 'react-router-dom'
import {
    Admission,
    ChangePassword,
    Home,
    Login,
    Register,
} from '../pages'

const ThemeRoutes = () => (
    <Switch>
        <Route path="/password/change">
            <ChangePassword />
        </Route>
        <Route path="/login">
            <Login />
        </Route>
        <Route path="/register">
            <Register />
        </Route>
        <Route path="/admission">
            <Admission />
        </Route>
        <Route to="/">
            <Home />
        </Route>
    </Switch>
)

export default ThemeRoutes