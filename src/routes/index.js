import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom'
import App from './../layouts/App'
import Theme from './../layouts/Theme'
import AppRoutes from './AppRoutes'
import ThemeRoutes from './ThemeRoutes'
import {
    Login
} from './../pages/Dashboard'

const Routes = () => (
    <Router>
        <Switch>
            {/* dashboard */}
            <Route path="/d/login">
                <Login />
            </Route>
            <Route path="/d">
                <App />
            </Route>

            {/* theme */}
            <Route path="/">
                <Theme />
            </Route>
        </Switch>
    </Router>
)

export default Routes

export {
    AppRoutes,
    ThemeRoutes
}