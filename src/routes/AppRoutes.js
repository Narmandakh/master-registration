import {
    Switch,
    Route,
    useRouteMatch
} from 'react-router-dom'
import {
    Admission,
    Admissions,
    Home,
} from '../pages/Dashboard'

const AppRoutes = () => {
    const { path } = useRouteMatch()

    return (
        <Switch>
            <Route path={`${path}/admissions/:id`}>
                <Admission />
            </Route>
            <Route path={`${path}/admissions`}>
                <Admissions />
            </Route>
            <Route path={path}>
                <Home />
            </Route>
        </Switch>
    )
}

export default AppRoutes