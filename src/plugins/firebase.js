import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyC4OjHR_P-6x8PUzj4KQJsfTv-juqq-YiY",
    authDomain: "master-2061b.firebaseapp.com",
    projectId: "master-2061b",
    storageBucket: "master-2061b.appspot.com",
    messagingSenderId: "539191571376",
    appId: "1:539191571376:web:398d506961f8ba2b0d5c75",
    measurementId: "G-K1EPDJB28M"
}

firebase.initializeApp(firebaseConfig)

export default firebase