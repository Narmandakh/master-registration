export const SET_STUDENT = 'SET_STUDENT'
export const REMOVE_STUDENT = 'REMOVE_STUDENT'

export const SET_ADMIN = 'SET_ADMIN'
export const REMOVE_ADMIN = 'REMOVE_ADMIN'