import {
    SET_STUDENT,
    REMOVE_STUDENT,
    SET_ADMIN,
    REMOVE_ADMIN
} from './actionTypes'

const initialState = {
    student: null,
    admin: null,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_STUDENT:
            const { student } = action.payload

            return {
                ...state,
                student
            }
        case REMOVE_STUDENT:
            return {
                ...state,
                student: null
            }
        case SET_ADMIN:
            const { admin } = action.payload

            return {
                ...state,
                admin
            }
        case REMOVE_ADMIN:
            return {
                ...state,
                admin: null
            }
        default:
            return state
    }
}

export default reducer