import {
    SET_STUDENT,
    REMOVE_STUDENT,
    SET_ADMIN,
    REMOVE_ADMIN,
} from './actionTypes'

const setStudent = student => ({
    type: SET_STUDENT,
    payload: {
        student
    }
})

const removeStudent = () => ({
    type: REMOVE_STUDENT
})

const setAdmin = admin => ({
    type: SET_ADMIN,
    payload: {
        admin
    }
})

const removeAdmin = () => ({
    type: REMOVE_ADMIN
})

export {
    setStudent,
    removeStudent,
    setAdmin,
    removeAdmin,
}