import {
    Box,
    Container,
    Grid,
    List,
    ListItem,
    ListItemText,
    Divider,
    Typography
} from '@material-ui/core'
import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'

const Footer = () => {
    const [items] = useState([
        {
            to: '/',
            label: 'Нүүр',
        },
        {
            to: '/admission',
            label: 'Элсэлт',
        },
        {
            to: '/works',
            label: 'Холбоо барих',
        },
    ])

    return (
        <Box py={12} bgcolor="#1b1d5a">
            <Container>
                <Grid
                    container
                    spacing={5}
                >
                    <Grid item xs={4}>
                        <img
                            width="50"
                            alt="logo"
                            src="/static/images/logo.png"
                        />
                        <Box color="#ffffff80" mt={3}>
                            <Typography>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis diam in mi sollicitudin dignissim. Ut a tincidunt quam. Ut bibendum arcu quam.</Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={4}>
                        <Box color="#fff">
                            <Box mb={3}>
                                <Typography variant="h5">Холбоос</Typography>
                            </Box>
                            <List component="nav">
                                {items.map(({ to, label }, index) =>
                                    <React.Fragment key={index}>
                                        <ListItem
                                            to={to}
                                            key={index}
                                            component={NavLink}
                                            button
                                            style={{
                                                backgroundColor: '#ffffff10'
                                            }}
                                        >
                                            <ListItemText primary={label} />
                                        </ListItem>
                                        <Divider />
                                    </React.Fragment>
                                )}
                            </List>
                        </Box>
                    </Grid>
                    <Grid item xs={4}>
                        <Box color="#fff" mb={3}>
                            <Typography variant="h5">Холбогдох</Typography>
                        </Box>
                        <Box color="#ffffff80">
                            <Box mb={2}>
                                <Typography>Хаяг: Улаанбаатар хот, СБД, VIII хороо, Бага тойруу-14, Улаанбаатар-48, Монгол Улс-210648</Typography>
                            </Box>
                            <Box mb={2}>
                                <Typography>Утас: 11311547</Typography>
                            </Box>
                            <Box>
                                <Typography>Цахим шуудан: info@msue.edu.mn</Typography>
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    )
}

export default Footer