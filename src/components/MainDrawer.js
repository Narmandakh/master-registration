import { connect } from 'react-redux'
import { removeAdmin } from './../redux/auth/actions'
import { useState } from 'react'
import { NavLink } from 'react-router-dom'
import {
    Avatar,
    Button,
    Drawer,
    List,
    ListItem,
    ListItemText
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import firebase from './../plugins/firebase'

const drawerWidth = 260

const useStyles = makeStyles({
    root: {
        width: drawerWidth,
        flexShrink: 0
    },
    drawerPaper: {
        width: drawerWidth,
        backgroundColor: '#43425d'
    },
    list: {
        paddingTop: 0,
        paddingBottom: 0
    },
    listItemActive: {
        position: 'relative',
        backgroundColor: '#00000030',

        '&:before': {
            content: "''",
            display: 'block',
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            width: 5,
            backgroundColor: '#a3a0fb'
        },
        '& $listItemIcon': {
            color: '#a3a0fb'
        }
    },
    listItemText: {
        color: '#fff',
        fontSize: 15
    },
    listItemIcon: {
        color: '#a5a4bf'
    },
    avatar: {
        width: 50,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: 0
    },
    button: {
        marginTop: 30,
        borderColor: 'rgba(255, 255, 255, 0.23)',
        color: '#fff'
    },
})

const MainDrawer = props => {
    const [items] = useState([
        {
            to: '/',
            label: 'Нүүр',
            exact: true
        },
        {
            to: '/admissions',
            label: 'Элсэгч',
            exact: false
        },
    ])

    const {
        root,
        drawerPaper,
        list,
        listItem,
        listItemText,
        listItemActive,
        avatar,
        button
    } = useStyles()

    const signOut = async () => {
        await firebase.auth().signOut()
        props.removeAdmin()
    }

    const userArea = (
        <div style={{ padding: 30 }}>
            <Avatar
                alt="Avatar"
                src="/static/images/logo.png"
                className={avatar}
            />
            <Button
                onClick={signOut}
                className={button}
                variant="outlined"
                fullWidth
            >
                LOGOUT
            </Button>
        </div>
    )

    const drawerContent = (
        <List
            className={list}
            component="nav"
        >
            {items.map(({ to, label, exact }, index) =>
                <ListItem
                    to={`/d${to}`}
                    key={index}
                    component={NavLink}
                    className={listItem}
                    activeClassName={listItemActive}
                    exact={exact}
                    button
                >
                    <ListItemText
                        primary={label}
                        classes={{
                            primary: listItemText,
                        }}
                    />
                </ListItem>
            )}
        </List>
    )

    return (
        <Drawer
            className={root}
            classes={{
                paper: drawerPaper
            }}
            variant="persistent"
            anchor="left"
            open
        >
            {userArea}
            {drawerContent}
        </Drawer>
    )
}

const mapDispatchToProps = {
    removeAdmin
}

export default connect(null, mapDispatchToProps)(MainDrawer)