import { connect } from 'react-redux'
import { useState } from 'react'
import { removeStudent } from './../redux/auth/actions'
import {
    NavLink,
    useHistory
} from 'react-router-dom'
import {
    Avatar,
    AppBar,
    Button,
    Container,
    Toolbar,
    Grid,
    List,
    ListItem,
    ListItemText,
    Menu,
    MenuItem
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { MoreVert } from '@material-ui/icons'

const useStyles = makeStyles({
    avatar: {
        width: 270,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: 0
    },
    appBar: {
        background: '#fff',
        boxShadow: 'none'
    },
    toolbar: {
        height: 80
    },
    list: {
        display: 'flex',
    },
    listItem: {
        color: '#00000090',
        width: 'auto',
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5,

        '&:hover': {
            backgroundColor: '#dbdcff50'
        }
    },
    listItemActive: {
        color: '#000',
        backgroundColor: '#dbdcff50'
    }
})

const Navbar = props => {
    const [proSelect, setProSelect] = useState(null)
    const [items] = useState([
        {
            to: '/',
            label: 'Нүүр',
            exact: true
        },
        {
            to: '/admission',
            label: 'Элсэлт',
            exact: false
        },
        {
            to: '/contact',
            label: 'Холбоо барих',
            exact: false
        },
    ])

    const history = useHistory()

    const {
        avatar,
        appBar,
        toolbar,
        list,
        listItem,
        listItemActive
    } = useStyles()

    const navbarLeft = (
        <Grid
            container
            spacing={2}
            alignItems="center"
        >
            <Grid item>
                <Avatar
                    alt="Avatar"
                    src="/static/images/logo2.png"
                    className={avatar}
                />
            </Grid>
            <Grid item>
                <List
                    component="nav"
                    className={list}
                >
                    {items.map(({ to, label, exact }, index) =>
                        <ListItem
                            to={to}
                            key={index}
                            component={NavLink}
                            className={listItem}
                            activeClassName={listItemActive}
                            exact={exact}
                            button
                        >
                            <ListItemText primary={label} />
                        </ListItem>
                    )}
                </List>
            </Grid>
        </Grid>
    )

    const navbarRight = (
        <Button
            variant="outlined"
            color="primary"
            size="large"
            to="/login"
            component={NavLink}
        >
            Нэвтрэх
        </Button>
    )

    const userProfile = (
        <>
            <Button
                variant="outlined"
                color="primary"
                size="large"
                onClick={e => setProSelect(e.currentTarget)}
                endIcon={<MoreVert />}
            >
                {props.student && props.student.email}
            </Button>
            <Menu
                anchorEl={proSelect}
                keepMounted
                open={Boolean(proSelect)}
                onClose={() => setProSelect(null)}
            >
                <MenuItem onClick={() => history.push('/password/change')}>
                    {'Нууц үг солих'}
                </MenuItem>
                <MenuItem onClick={props.removeStudent}>
                    {'Гарах'}
                </MenuItem>
            </Menu>
        </>
    )

    return (
        <AppBar
            className={appBar}
            position="static"
        >
            <Toolbar className={toolbar}>
                <Container>
                    <Grid
                        container
                        justify="space-between"
                        alignItems="center"
                    >
                        <Grid item>
                            {navbarLeft}
                        </Grid>
                        <Grid item>
                            {props.student ? userProfile : navbarRight}
                        </Grid>
                    </Grid>
                </Container>
            </Toolbar>
        </AppBar>
    )
}

const mapStateToProps = state => ({
    student: state.student
})

const mapDispatchToProps = {
    removeStudent
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navbar)