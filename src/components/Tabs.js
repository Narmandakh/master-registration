import React from 'react'
import PropTypes from 'prop-types'
import {
    AppBar,
    Tabs as BaseTabs,
    Tab,
    Typography,
    Box,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    )
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
}

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    }
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={(event) => {
                event.preventDefault()
            }}
            {...props}
        />
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}))

const Tabs = ({ items, panels }) => {
    const classes = useStyles()
    const [value, setValue] = React.useState(0)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    return (
        <div className={classes.root}>
            <BaseTabs
                variant="fullWidth"
                value={value}
                onChange={handleChange}
                aria-label="nav tabs example"
                indicatorColor="primary"
            >
                {items.map((item, index) =>
                    <LinkTab
                        label={item}
                        {...a11yProps(index)}
                    />
                )}
            </BaseTabs>
            {panels.map((panel, index) =>
                <TabPanel
                    value={value}
                    index={index}
                >
                    {panel}
                </TabPanel>
            )}
        </div>
    )
}

export default Tabs